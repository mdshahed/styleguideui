# Set the base image to node:12-alpine
FROM node:12 as react-build

# Specify where our app will live in the container
WORKDIR /builddir

# Copy the React App to the container
COPY . ./

# Prepare the container for building React
RUN npm install
RUN npm install react-scripts@3.0.1 -g
# We want the production version
RUN npm run build

# Prepare nginx
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build /builddir/build /usr/share/nginx/html

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
