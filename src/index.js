import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Navbar from './Components/Navbar/Navbar';
import reportWebVitals from './reportWebVitals';
import StyleGuide from './Components/Main/Styleguide';
import ToggleSwitch from './Components/Brand-toggle/Brand-toggle';
import Sidebar from './Components/Sidebar/Sidebar';
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import Typography from './Components/Typography/Typography';
import Branding from './Components/Branding/Branding';
import Colors from './Components/Colors/Colors';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Navbar />
      <StyleGuide />
      {/* <ToggleSwitch
        values={['Both', 'Catalyte', 'Surge']}
        selected="Catalyte"
      /> */}
      <Sidebar />
      <Switch>
        <Route exact path="/style_guide/typography" component={Typography} />
        <Route exact path="/style_guide/branding" component={Branding} />
        <Route exact path="/style_guide/colors" component={Colors} />
      </Switch>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
