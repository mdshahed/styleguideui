import './Sidebar.css';

function Sidebar() {
  return (
    <sidebar-menu>
      <li>
        <a href="/style_guide/colors" data-cy="colors-nav-link">
          Colors
        </a>
      </li>
      <li>
        <a href="/style_guide/typography" data-cy="typography-nav-link">
          Typography
        </a>
      </li>
      <li>
        <a href="/style_guide/branding" data-cy="branding-nav-link">
          Branding
        </a>
      </li>
    </sidebar-menu>
  );
}

export default Sidebar;
