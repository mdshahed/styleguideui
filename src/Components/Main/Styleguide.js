import './Styleguide.css';

function StyleGuide() {
  return (
    <header-container class="home-container">
      <header-img class="header-img">
        <div class="header-text-box" data-cy="styleguide-header">
          Catalyte Style Guide
        </div>
      </header-img>
    </header-container>
  );
}

export default StyleGuide;
