import './ColorPalette.css';
import ClipboardJS from 'clipboard';
import { useState } from 'react';

function ColorPalette(props) {
  const { colorName } = props;
  const { colorStyleName } = props;
  const { colorHexValue } = props;
  const [hoverText, setHoverText] = useState('Copy to Clipboard');
  const confirmCopy = () => {
    setHoverText('Copied!');
  };
  const unHoverText = () => {
    setHoverText('Copy to ClipBoard');
  };
  new ClipboardJS('.copy-clipboard-btn');

  return (
    <div class="color-palette-container">
      <div class={'color-preview ' + colorStyleName}>
        <button
          class="copy-clipboard-btn hide"
          data-clipboard-text={colorHexValue}
          onClick={confirmCopy}
          onMouseLeave={unHoverText}
        >
          <h4>{hoverText}</h4>
        </button>
      </div>
      <div class="color-info">
        <b class={'color-name'}>{colorName}</b>
        <div class="color-hex-name">{colorHexValue}</div>
      </div>
    </div>
  );
}

export default ColorPalette;
