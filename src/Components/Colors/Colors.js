import ColorPalette from './ColorPalette';
import './Colors.css';

function Colors() {
  const odysseyColors = [
    {
      colorStyleName: 'midnight-blue',
      colorName: 'Midnight Blue',
      colorHexValue: '#1B3E77',
    },
    {
      colorStyleName: 'medium-turqoise',
      colorName: 'Medium Turqoise',
      colorHexValue: '#65BBD3',
    },
    {
      colorStyleName: 'light-coral',
      colorName: 'Light Coral',
      colorHexValue: '#F4708F',
    },

    {
      colorStyleName: 'white-smoke',
      colorName: 'White Smoke',
      colorHexValue: '#F8F8F8',
    },
    {
      colorStyleName: 'off-white',
      colorName: 'Off White',
      colorHexValue: '#FCFCFC',
    },
    {
      colorStyleName: 'light-slate-gray',
      colorName: 'Light Slate Gray',
      colorHexValue: '#919191',
    },
  ];
  return (
    <div class="page-body">
      <h1>Colors</h1>
      <hr />
      <h2>Brand Colors</h2>

      <div className="palette-grid">
        <ColorPalette
          colorStyleName="midnight-blue"
          colorName="Midnight Blue"
          colorHexValue="#1B3E77"
        />
        <ColorPalette
          colorStyleName="medium-turqoise"
          colorName="Medium Turqoise"
          colorHexValue="#65BBD3"
        />
        <ColorPalette
          colorStyleName="light-coral"
          colorName="Light Coral"
          colorHexValue="#F4708F"
        />
        <ColorPalette
          colorStyleName="white-smoke"
          colorName="White Smoke"
          colorHexValue="#F8F8F8"
        />
        <ColorPalette
          colorStyleName="off-white"
          colorName="Off White"
          colorHexValue="#FCFCFC"
        />
        <ColorPalette
          colorStyleName="light-slate-gray"
          colorName="Light Slate Gray"
          colorHexValue="#919191"
        />
      </div>
    </div>
  );
}

export default Colors;
