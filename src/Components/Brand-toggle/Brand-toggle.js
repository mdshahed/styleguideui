import React, { Component } from 'react';
import { render } from 'react-dom';
import './brand-toggle-styles.css';

const titleCase = (str) =>
  str
    .split(/\s+/)
    .map((w) => w[0].toUpperCase() + w.slice(1))
    .join(' ');

const ClickableLabel = ({ title, onChange, id }) => (
  <label
    class="switch-label"
    htmlFor={title}
    value={title}
    onClick={() => onChange(title)}
    className={id}
    data-cy="toggle-switch"
  >
    {titleCase(title)}
  </label>
);

const ConcealedRadio = ({ value, selected }) => (
  <input
    class="switch-radio"
    type="radio"
    id={value}
    name="switch"
    defaultChecked={selected === value}
  />
);

class ToggleSwitch extends Component {
  state = {
    selected: this.props.selected,
    selectedcolor: this.props.selectedcolor,
  };

  handleChange = (val) => {
    this.setState({ selected: val, selectedcolor: 'color: white' });
  };

  selectionStyle = () => {
    return {
      left: `${(this.props.values.indexOf(this.state.selected) / 3) * 100}%`,
    };
  };

  render() {
    const { selected } = this.state;
    return (
      <switch-container class="switch">
        {this.props.values.map((val) => {
          return (
            <span>
              <ConcealedRadio value={val} name={val} selected={selected} />
              <ClickableLabel title={val} onChange={this.handleChange} />
            </span>
          );
        })}
        <span class="switch-span" style={this.selectionStyle()} />
      </switch-container>
    );
  }
}

export default ToggleSwitch;
