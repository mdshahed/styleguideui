// import './Typography.css';
import './Typography.scss';
import Infobox from './Infobox';
import Sampletext from './Sampletext';
import Example from './Example';

/**
 * @name Typography
 * @description Typography page that displays the different fonts and styles at use in the guide
 * @returns html element
 */
const Typography = () => {
  return (
    <div class="page-body" data-cy="page-body">
      <h1 data-cy="page-heading">Typography</h1>
      <hr />

      <h2 data-cy="page-subheading1">Fonts</h2>

      <div id="Montserrat" data-cy="montserrat">
        <h3 data-cy="mont-title">Montserrat</h3>
        <table id="mont-table" class="font-table" data-cy="mont-table">
          <Sampletext weightType="400" data-cy="mont400" />

          <Sampletext weightType="500" data-cy="mont500" />

          <Sampletext weightType="700" data-cy="mont700" />

          <Sampletext weightType="900" data-cy="mont900" />
        </table>
        <Infobox
          normalFont={true}
          htmlLink={
            '<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">'
          }
          cssLink={'font-family: "Montserrat", sans-serif;'}
          data-cy="mont-box"
        />
      </div>

      <div id="Nunito" data-cy="nunito">
        <h3 data-cy="nun-title">Nunito</h3>
        <table id="nun-table" class="font-table" data-cy="nun-table">
          <Sampletext weightType="400" data-cy="nun400" />

          <Sampletext weightType="600" data-cy="nun600" />

          <Sampletext weightType="700" data-cy="nun700" />

          <Sampletext weightType="900" data-cy="nun900" />
        </table>
        <Infobox
          normalFont={true}
          htmlLink={
            '<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">'
          }
          cssLink={'font-family: "Nunito", sans-serif'}
          data-cy="nun-box"
        />
      </div>

      <div class="Sysui" data-cy="sys-ui">
        <h3 data-cy="sys-title">System-UI</h3>
        <table id="sys-table" class="font-table" data-cy="sys-table">
          <Sampletext weightType="300" data-cy="sys300" />

          <Sampletext weightType="400" data-cy="sys400" />
        </table>
        <Infobox
          normalFont={false}
          cssLink={
            "font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
          }
          data-cy="sys-box"
        />
      </div>

      <div>
        <h2 data-cy="page-subheading2">Styles</h2>

        <Example
          header="Learning Page - Title"
          bgColor="#FFF"
          textColor="#212519"
          textFamily="system-ui"
          textSize="35 px"
          textWeight="300 Light"
          data-cy="rating"
        />

        {/* H2 */}
        <Example
          header="My Dashboard - Coaching Sessions and Current Skill"
          bgColor="#F8F8F8"
          textColor="#1B3E77"
          textFamily="Nunito"
          textSize="26 px"
          textWeight="900 Black"
          data-cy="coaching-sessions"
        />

        {/* H3 */}
        <Example
          header="Learning Page - Assignment Title"
          bgColor="#FFF"
          textColor="#5a5a5a"
          textFamily="system-ui"
          textSize="24.5 px"
          textWeight="300 Light"
          data-cy="assignment"
        />

        {/*H5 */}
        <Example
          header="My Dashboard - Current Skill"
          bgColor="#FCFCFC"
          textColor="#919191"
          textFamily="Nunito"
          textSize="16 px"
          textWeight="700 Bold"
          data-cy="skill-name"
        />

        {/*li in nav */}
        <Example
          header="Learning Page - Path"
          bgColor="#FFF"
          textColor="#212519"
          textFamily="system-ui"
          textSize="14 px"
          textWeight="400 Normal"
          data-cy="path"
        />

        {/*a*/}
        <Example
          header="My Dashboard - Selected Navbar Heading"
          bgColor="#1B3E77"
          textColor="#FFF"
          textFamily="Montserrat"
          textSize="13.5 px"
          textWeight="700 Bold"
          data-cy="selected-heading"
        />

        {/*a*/}
        <Example
          header="My Dashboard - Unselected Navbar Heading"
          bgColor="#1B3E77"
          textColor="#FFF"
          textFamily="Montserrat"
          textSize="13.5 px"
          textWeight="400 Normal"
          data-cy="unselected-heading"
        />

        {/*span */}
        <Example
          header="My Dashboard - Tool Tip and Example"
          bgColor="#FFF"
          textColor="#22447B"
          textFamily="Nunito"
          textSize="12 px"
          textWeight="700 Bold"
          data-cy="coaching-tip"
        />

        {/*span */}
        <Example
          header='My Dashboard - Active "Show Comment" Toggle'
          bgColor="#FFF"
          textColor="#838383"
          textFamily="Nunito"
          textSize="12 px"
          textWeight="400 Normal"
          data-cy="existing-comment-dropdown"
        />

        {/*span */}
        <Example
          header='My Dashboard - Inactive "Show Comment" Toggle'
          bgColor="#FFF"
          textColor="#C5C5C5"
          textFamily="Nunito"
          textSize="12 px"
          textWeight="400 Normal"
          data-cy="no-comment-dropdown"
        />

        {/*span */}
        <Example
          header="Learning Page - Assignment Stats"
          bgColor="#FFF"
          textColor="#6c757d"
          textFamily="system-ui"
          textSize="12.25 px"
          textWeight="400 Normal"
          data-cy="stats"
        />

        {/*div*/}
        <Example
          header="My Dashboard - Navbar User Greeting"
          bgColor="#1B3E77"
          textColor="#FFF"
          textFamily="Montserrat"
          textSize="13.5 px"
          textWeight="500 Medium"
          data-cy="greeting-heading"
        />

        {/*div */}
        <Example
          header="My Dashboard - User Name"
          bgColor="#F8F8F8"
          textColor="#1B3E77"
          textFamily="Montserrat"
          textSize="24 px"
          textWeight="900 Black"
          data-cy="username"
        />

        {/*div */}
        <Example
          header="My Dashboard - User Position"
          bgColor="#F8F8F8"
          textColor="#919191"
          textFamily="Nunito"
          textSize="12.7 px"
          textWeight="600 Semi Bold"
          data-cy="userpos"
        />

        {/*div */}
        <Example
          header="My Dashboard - Tip Description"
          bgColor="#FFF"
          textColor="#919191"
          textFamily="Nunito"
          textSize="12 px"
          textWeight="400 Normal"
          data-cy="coaching-description"
        />

        {/*div */}
        <Example
          header="My Dashboard - Apprentice and Coach Assessment Heading"
          bgColor="#FCFCFC"
          textColor="#22447B"
          textFamily="Nunito"
          textSize="14 px"
          textWeight="700 Bold"
          data-cy="assessment-owner"
        />

        {/*div */}
        <Example
          header="My Dashboard - Rating"
          bgColor="#FCFCFC"
          textColor="#aaa"
          textFamily="system-ui"
          textSize="9 px"
          textWeight="400 Normal"
          data-cy="rating"
        />
      </div>
    </div>
  );
};

export default Typography;
