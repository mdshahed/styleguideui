/**
 * @name Sampletext
 * @description a wrapper element for sample text to display intended style
 * @returns html element
 */
const Sampletext = (props) => {
  const { weightType } = props;

  const sampleTextStyle = {
    fontWeight: weightType,
  };

  const letterSample = 'Aa';
  const uppercaseSample = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowercaseSample = uppercaseSample.toLowerCase();
  const numsspecialSample = '1234567890(,.;:?!$&*)';

  return (
    <td style={sampleTextStyle}>
      <div class="center-list">
        <div class="weightnum">Weight {weightType}</div>
        <div class="left-list">
          <ul>{letterSample}</ul>
          <ul>{uppercaseSample}</ul>
          <ul>{lowercaseSample}</ul>
          <ul>{numsspecialSample}</ul>
        </div>
      </div>
    </td>
  );
};

export default Sampletext;
