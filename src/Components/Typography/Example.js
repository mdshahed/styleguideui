/**
 * @name Example
 * @description a wrapper element for a full Example type.
 * @param {Object} props data passed down to component
 * @returns html element
 */
const Example = (props) => {
  // logic to make each loremipsum the style of properties

  const loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
  const {
    header,
    textColor,
    bgColor,
    textFamily,
    textSize,
    textWeight,
  } = props;

  const exampleStyle = {
    color: textColor,
    backgroundColor: bgColor,
    fontSize: textSize.replace(/\s+/g, ''),
    fontFamily: textFamily,
    fontWeight: textWeight.split(' ')[0],
  };

  return (
    <table class="font-table">
      <tr>
        <td class="label-text">{header}</td>
        <td class="lorem-text" style={exampleStyle}>
          {loremIpsum}
        </td>
        <td>
          <ul>
            Text Color: <b>{textColor}</b>
          </ul>
          <ul>
            Background Color: <b>{bgColor}</b>
          </ul>
          <ul>
            Font: <b>{textFamily}</b>
          </ul>
          <ul>
            Size: <b>{textSize}</b>
          </ul>
          <ul>
            Weight: <b>{textWeight}</b>
          </ul>
        </td>
      </tr>
    </table>
  );
};

export default Example;
