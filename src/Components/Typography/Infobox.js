import React, { useState } from 'react';

/**
 * @name Infobox
 * @description a wrapper element for two textarea features including html and css information
 * @param {Object} props data passed down to component
 * @returns html element
 */
const Infobox = (props) => {
  const { htmlLink, cssLink, normalFont } = props;
  const [htmlTitle, setHtmlTitle] = useState('HTML');
  const [cssTitle, setCssTitle] = useState('CSS');

  /**
   * @name copyText
   * @description allows for text to be copied, with a confirmation message after click.
   * @param {String} text string in textarea
   * @param {String} title string above textarea (subtitle)
   */
  const copyText = (text, title) => {
    navigator.clipboard.writeText(text);
    if (title.includes('HTML')) {
      setHtmlTitle('Copied!');
    }

    if (title.includes('CSS')) {
      setCssTitle('Copied!');
    }
  };

  /**
   * @name hoverText
   * @description changes subtitle to notify user of click to copy functionaltiy.
   * @param {*} title string above textarea (subtitle)
   */
  const hoverText = (title) => {
    if (title == 'HTML') {
      setHtmlTitle('Click to copy HTML to clipboard');
    }

    if (title == 'CSS') {
      setCssTitle('Click to copy CSS to clipboard');
    }
  };

  /**
   * @name unHoverText
   * @description reverts subtitle back to original defaut when mouse moves off textarea.
   */
  const unHoverText = () => {
    setHtmlTitle('HTML');
    setCssTitle('CSS');
  };

  return (
    <div>
      {normalFont && (
        <div>
          <h4 class="title">{htmlTitle}</h4>
          <textarea
            readOnly
            rows="2"
            cols="100"
            onClick={() => copyText(htmlLink, htmlTitle)}
            onMouseOver={() => hoverText(htmlTitle)}
            onMouseLeave={() => unHoverText()}
          >
            {htmlLink}
          </textarea>
        </div>
      )}
      <div>
        <h4 class="title">{cssTitle}</h4>
        <textarea
          readOnly
          rows="2"
          cols="100"
          onClick={() => copyText(cssLink, cssTitle)}
          onMouseOver={() => hoverText(cssTitle)}
          onMouseLeave={() => unHoverText()}
        >
          {cssLink}
        </textarea>
      </div>
    </div>
  );
};

export default Infobox;
