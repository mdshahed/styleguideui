import logo from './catalyte.png';
import './Navbar.css';

function Navbar() {
  return (
    <navbar-fixed>
      <navbar-flex>
        <a href="/">
          <img src={logo} alt="" />
        </a>
        <navbar-links>
          <li>
            <a href="/registration_page" data-cy="registration-nav-link">
              Registration Page
            </a>
          </li>
          <li>
            <a href="/style_guide" data-cy="styleguide-nav-link">
              Style Guide
            </a>
          </li>
          <li>
            <a href="/component_library" data-cy="component_library-nav-link">
              Component Library
            </a>
          </li>
          <li>
            <a href="/contact" data-cy="contact-nav-link">
              Contact
            </a>
          </li>
        </navbar-links>
      </navbar-flex>
    </navbar-fixed>
  );
}

export default Navbar;
