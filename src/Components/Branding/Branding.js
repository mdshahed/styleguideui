// imports go here

import './Branding.css';

import Catalyte from './sections/Catalyte';
import LogoVariation from './sections/LogoVariation';
import Icons from './sections/Icons';
import ClientLogos from './sections/ClientLogos';

const Branding = () => {
  return (
    <div class="page-body">
      <h1 data-cy="brand-page-header">Brand / Logo</h1>
      <hr />
      <div class="sections">
        <Catalyte></Catalyte>
        <LogoVariation></LogoVariation>
        <Icons></Icons>
        <ClientLogos></ClientLogos>
      </div>
    </div>
  );
};

export default Branding;
