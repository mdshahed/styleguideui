import { ReactComponent as LogoVariationsLogoBlack } from '../../../assets/images/logo-variations/logo_black.svg';
import { ReactComponent as LogoVariationsLogoUrl } from '../../../assets/images/logo-variations/logo_url.svg';
import { ReactComponent as LogoVariationsLogoUrlHq } from '../../../assets/images/logo-variations/logo_url_hq.svg';
import { ReactComponent as LogoVariationsLogoWhite } from '../../../assets/images/logo-variations/logo_white.svg';

const LogoVariation = () => {
  return (
    <section>
      <h2>Logo Variation</h2>
      <div class="img-wrapper">
        <LogoVariationsLogoBlack class="img-center"></LogoVariationsLogoBlack>
      </div>
      <br></br>
      <div class="img-wrapper">
        <LogoVariationsLogoUrl class="img-center"></LogoVariationsLogoUrl>
      </div>
      <br></br>
      <div class="img-wrapper">
        <LogoVariationsLogoUrlHq class="img-full-width img-center"></LogoVariationsLogoUrlHq>
      </div>
      <br></br>
      <div class="img-wrapper-with-background-brown">
        <LogoVariationsLogoWhite class="img-with-background img-center"></LogoVariationsLogoWhite>
      </div>
    </section>
  );
};

export default LogoVariation;
