import { ReactComponent as CatalyteLogoTransparent } from '../../../assets/images/catalyte/catalyte_logo_transparent.svg';

const Catalyte = () => {
  return (
    <section>
      <h2>CATALYTE</h2>
      <CatalyteLogoTransparent
        data-cy="catalyte-logo-transparent"
        class="img-full-width img-center"
      ></CatalyteLogoTransparent>
    </section>
  );
};

export default Catalyte;
