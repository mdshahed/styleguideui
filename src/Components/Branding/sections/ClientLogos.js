import logoClientLogosAtt from '../../../assets/images/client-logos/at&t.svg';
import logoClientLogosAetna from '../../../assets/images/client-logos/aetna.svg';
import logoClientLogosAaa from '../../../assets/images/client-logos/aaa.svg';
import logoClientLogosNike from '../../../assets/images/client-logos/nike.svg';
import logoClientLogosMicrosoft from '../../../assets/images/client-logos/microsoft.svg';
import logoClientLogosStubHub from '../../../assets/images/client-logos/stub_hub.svg';
import logoClientLogosGss from '../../../assets/images/client-logos/good_samaritan_society.svg';
import logoClientLogosChs from '../../../assets/images/client-logos/cambia_health_solutions.svg';
import logoClientLogosGeico from '../../../assets/images/client-logos/geico.svg';
import logoClientLogosVistana from '../../../assets/images/client-logos/vistana.svg';
import logoClientLogosUa from '../../../assets/images/client-logos/under_armour.svg';
import logoClientLogosJhm from '../../../assets/images/client-logos/johns_hopkins_medicine.svg';
import logoClientLogosUw from '../../../assets/images/client-logos/university_of_washington.svg';
import logoClientLogosRedHat from '../../../assets/images/client-logos/red_hat.svg';

const ClientLogos = () => {
  return (
    <section>
      <h2 data-cy="client-logos-header">Client Logos</h2>
      <ul class="b-imagegrid__grid">
        <li class="b-imagegrid__item">
          <img src={logoClientLogosAtt} data-cy="att-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosAetna} data-cy="aetna-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosAaa} data-cy="aaa-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosNike} data-cy="nike-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosMicrosoft} data-cy="microsoft-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosStubHub} data-cy="stubhub-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosGss} data-cy="gss-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosChs} data-cy="chs-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosGeico} data-cy="geico-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosVistana} data-cy="vistana-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosUa} data-cy="ua-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosJhm} data-cy="jhm-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosUw} data-cy="uw-logo"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoClientLogosRedHat} data-cy="redhat-logo"></img>
        </li>
      </ul>
    </section>
  );
};

export default ClientLogos;
