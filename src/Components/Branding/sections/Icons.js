import logoIconsIcon1 from '../../../assets/images/icons/icon_1.svg';
import logoIconsIconBlack from '../../../assets/images/icons/icon_black.svg';
import logoIconsIconWhite from '../../../assets/images/icons/icon_white.svg';
import logoIconsIcon from '../../../assets/images/icons/icon.svg';

const Icons = () => {
  return (
    <section>
      <h2 data-cy="icons-header">Icons</h2>
      <ul class="b-imagegrid__grid">
        <li class="b-imagegrid__item">
          {' '}
          <img src={logoIconsIcon1} data-cy="icon-1"></img>
        </li>
        <li class="b-imagegrid__item">
          <img src={logoIconsIconBlack} data-cy="icon-black"></img>
        </li>
        <li class="b-imagegrid__item img-wrapper-with-background-orange">
          <img src={logoIconsIconWhite} data-cy="icon-white"></img>
        </li>
        <li class="b-imagegrid__item">
          <img
            src={logoIconsIcon}
            class="img-full-width"
            data-cy="icon-full"
          ></img>
        </li>
      </ul>
    </section>
  );
};

export default Icons;
