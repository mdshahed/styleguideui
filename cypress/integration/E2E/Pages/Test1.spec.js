describe('Load Home Page', () => {
  beforeEach(function () {
    cy.viewport(1280, 800);
    cy.visit('http://localhost:3000');
  });

  it('Verify Home page load properly', () => {
    cy.contains('Catalyte Style Guide');
    cy.url().should('include', 'localhost:3000');
    cy.get('[data-cy=typography-nav-link]').click();
    cy.go('back');
    cy.get('[data-cy=branding-nav-link]').click();
  });
});
