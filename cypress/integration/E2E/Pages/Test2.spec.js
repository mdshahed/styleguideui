describe('Load Branding Page', () => {
  beforeEach(function () {
    cy.viewport(1280, 800);
    cy.visit('http://localhost:3000');
  });

  it('Verify Branding page load properly', () => {
    cy.get('[data-cy=branding-nav-link]').click();
    cy.get('[data-cy=brand-page-header]');
    cy.url().should('include', 'localhost:3000/style_guide/branding');
    cy.get('[data-cy=catalyte-logo-transparent]');
  });
});
