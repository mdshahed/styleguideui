describe('Load Typography Page', () => {
  beforeEach(function () {
    cy.viewport(1280, 800);
    cy.visit('http://localhost:3000');
  });

  it('Verify Typography page load properly', () => {
    cy.get('[data-cy=typography-nav-link]').click();
    cy.get('[data-cy=page-heading]');
    cy.url().should('include', 'localhost:3000/style_guide/typography');
  });
});
