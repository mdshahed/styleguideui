describe('Load Colors Page', () => {
  beforeEach(function () {
    cy.viewport(1280, 800);
    cy.visit('http://localhost:3000');
  });

  it('Verify Colors page load properly', () => {
    cy.get('[data-cy=colors-nav-link]').click();
    //cy.get('[data-cy=colors-page-header]');
    cy.url().should('include', 'localhost:3000/style_guide/colors');
  });
});
